import {loadScript, loadScripts} from "./js/utils.js";

const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js",
    "vendors/js/markdown-it-span.js",
];

await loadScripts(scritpList);

const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
const partsList = ["cover", 'content', "back"];

const markdownit = window.markdownit(
    {
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer)
    .use(markdownitSpan)
    .use(markdownItAttrs, {
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

function init() {

    mdFilesList.forEach(async (file, index) => {

        const reponse = await fetch(file);
        const mdContent = await reponse.text();

        const result = markdownit.render(mdContent);

        document.getElementById(partsList[index]).innerHTML = result;

        if (index === mdFilesList.length - 1) {
            await loadScript("vendors/js/paged.polyfill.js")
        }
    });

    //add the paged.js css
    var pagedCss = document.createElement("link");
    pagedCss.href = "vendors/css/paged-preview.css";
    pagedCss.type = "text/css";
    pagedCss.rel = "stylesheet";
    document.head.appendChild(pagedCss);
}

init();
