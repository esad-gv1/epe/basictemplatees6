# EPE - Ecran-Papier-Editer

## Simple Markdown to Paged.js sample

The Markdown format is used to enable the rapid manipulation of long texts. It's a first introduction to web technologies for producing formatted objects intended for printing.
Paged.js takes care of the layout in the browser. A PDF can then be printed using the print function and by choosing PDF as the print destination.

[https\://gitlab.com/esad-gv1/epe/templatepagedjs_ws](https://gitlab.com/esad-gv1/epe/templatepagedjs_ws)

## Usage

Download this folder (in zip format, for example).

### Modify text and image content

Modify files in the md folder\:

- `md/cover.md` contains data dedicated to the cover page, designed to fit on a single page,
- `md/main.md` contains the main content of the book, which will be paginated automatically,
- `md/back.md` contains the 4th cover.

### Defining styles

To define styles for the pages and their content, you need to modify the `css` files corresponding to the `.md` file:

- `css/cover.css`
- `css/style.css`
- `css/back.css`

The `css/fonts` folder contains the font files to be used in the project.

## Markdown - Specific extensions used in this template

### Attributes, div and span

Several plugins are available with markdown-it for structuring content. 

#### Add a `class` or an `id`.
In `markdown`\:
```md
## Heading 2{#first-part}

This is a paragraph{.content}
```
`html` conversion\:

```html
<h2 id="first-part">Title 2</h2>
<p class=" content">This is a paragraph</p>
```

#### Div
To create a div to group several content elements in `markdown`\: 
```md
:::
## Heading 2

This is a paragraph.

This is another paragraph.
:::
```

`html` conversion\:

```html
<div>
    <h2>Titre 2</h2>
    <p>Ceci est un paragraphe.</p>
    <p>Ceci est un autre paragraphe.</p>
</div>
```

To add a class or a `#id` to your `.div`, follow the synthax below:
```md
::: .class
## Heading 2

This is a paragraph.

This is another paragraph.
:::

::: #id
## Heading 2

This is a paragraph.

This is another paragraph.
:::

```
#### Span

To add spans, use the following syntax:

`This is a paragraph with :something: to isolate.`

It's possible to add a `class` or an `id`:

`This is a :paragraph:{#id} with :something:{.class} to isolate.`

`html` conversion\:
```html
<p>This is a <span id="id"></span> with some <span class="class">thing</span> to isolate</p>
```

## Technical choices

```{.tree}
.
├── css
│   ├── back.css
│   ├── cover.css
│   ├── fonts
│   │   └── Inter-VariableFont_slnt_wght.ttf
│   └── style.css
├── index.html
├── js
│   ├── test.js
│   └── utils.js
├── md
│   ├── back.md
│   ├── cover.md
│   └── main.md
├── private
├── readme.md
├── script.js
└── vendors
    ├── css
    │   ├── paged-preview.css
    │   └── pagedjs-interface.css
    └── js
        ├── markdown-it-attrs.js
        ├── markdown-it-bracketed-spans.js
        ├── markdown-it-container.js
        ├── markdown-it-footnote.js
        ├── markdown-it-span.js
        ├── markdown-it.js
        ├── paged.polyfill-2.0.js
        └── paged.polyfill.js
```

This template uses a more modern ES6 structure. `index.html` file uses a ```<script>``` tag to import only one main scripts :

```html
<script src="script.js" type="module" defer></script>
```

The 3 parts that compose the printed booklet are defined by 3 HTML `div` elements with specific classes and id for each : `cover`,`content`,`back`. These are used to define styles in the css files.

The `script.js` file contains the script that instanciates and configure markdown-it and its plug-ins, loads the content of the 3 markdown files that the user edits, then loads the Paged.js polyfill that will automatically get the body content to paginate and preview it.
Whereever possible, `import` mechanism is used to get a more flexible structure for users who would like to extend this template by their own.

## Credits

- [Markdown-it](https://www.npmjs.com/package/markdown-it) for converting markdown to html in the browser;
- [Paged.js](https://pagedjs.org) for printed page layout and pdf generation;

## Authors
Dominique Cunin, Quentin Juhel, Cédric Rossignol-Brunet